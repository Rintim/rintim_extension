// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From Theopse (Self@theopse.org)
// Licensed under BSD-2-Caluse
// File: extension.js (rExtension/Rain/extension.js)
// Content:  
// Copyright (c) 2022 Theopse Organization All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

"use strict"

game.import("extension", (lib, game, ui, get, ai, _status) => {
    let intro = "Version: Build 0 Form 1</br>- - - - - - - - - - - - - - - - - - - - - - - - -</br>●前置需求: </br>- reHeart</br>- StandardLib</br></br>●注意: <span style=\"color:#FF3333\">你目前在使用Development分支</br>该分支下可能会出现BUG，请确认需求使用</span></br>"
    let extension = {
        name: "雨落同清",
        editable: false,
        content: (config, pack) => {
            // Set Menu
            {
                let name = "雨落同清"
                lib.extensionMenu[`extension_${name}`]["title"] = {
                    "name": "<p align=center><span style=\"font-size:18px\">- - - - - - - - - - - - - - - - -</span>",
                    "clear": true,
                    "nopointer": true
                };

                lib.extensionMenu[`extension_${name}`]["character_enable"] = {
                    name: "武将启用",
                    intro: "开启此功能后重启生效。启用扩展包中的武将。",
                    init: true,
                };
            }
            // Add Character
            if (config.character_enable) {
                let characters = {
                    "rExtension_Rain_Character_liubei": {
                        sex: "male",
                        group: "shu",
                        hp: 4,
                        replace: "liubei",
                        maxHp: 4,
                        rank: "ap",
                        translate: "刘备",
                        skills: ["rExtension_Rain_Skill_liubei_rende"],
                        tags: [
                            "zhu",
                            "ext:雨落同清/image/character/rExtension_Rain_Character_liubei.jpg",
                            "die:ext:雨落同清/audio/die",
                        ],
                        des: "先主姓刘，讳备，字玄德，涿郡涿县人，汉景帝子中山靖王胜之后也。以仁德治天下。",
                        catelogy: "Standard"
                    },
                    "rExtension_Rain_Character_simayi": {
                        sex: "male",
                        group: "wei",
                        hp: 3,
                        replace: "simayi",
                        maxHp: 4,
                        rank: "ap",
                        translate: "司马懿",
                        skills: ["rExtension_Rain_Skill_simayi_fankui", "rExtension_Rain_Skill_simayi_guicai"],
                        tags: [
                            "ext:雨落同清/image/character/rExtension_Rain_Character_simayi.jpg",
                            "die:ext:雨落同清/audio/die",
                        ],
                        des: "晋宣帝，字仲达，河内温人。曾任职过曹魏的大都督，太尉，太傅。少有奇节，聪明多大略，博学洽闻，伏膺儒教，世之鬼才也。",
                        catelogy: "Standard"
                    },
                    "rExtension_Rain_Character_guojia": {
                        sex: "male",
                        group: "wei",
                        hp: 3,
                        replace: "guojia",
                        maxHp: 3,
                        rank: "ap",
                        translate: "郭嘉",
                        skills: ["rExtension_Rain_Skill_guojia_tiandu", "rExtension_Rain_Skill_guojia_yiji"],
                        tags: [
                            "ext:雨落同清/image/character/rExtension_Rain_Character_guojia.jpg",
                            "die:ext:雨落同清/audio/die",
                        ],
                        des: "字奉孝，颍川阳翟人，官至军师祭酒。惜天妒英才，英年早逝。有诗云：“良计环环不遗策，每临制变满座惊”",
                        catelogy: "Standard"
                    }
                }

                lib.characterPack.mode_extension_rExtension_Rain = {};

                for (let name in characters) {
                    let info = characters[name]
                    let showHp;
                    if (!info.maxHp || info.maxHp == info.hp) showHp = info.hp;
                    else showHp = `${info.hp}/${info.maxHp}`;
                    let character = [info.sex, info.group, showHp, info.skills, info.tags];
                    lib.character[name] = character;
                    lib.translate[name] = info.translate;
                    lib.characterPack.mode_extension_rExtension_Rain[name] = character;
                    if (info.rank) {
                        lib.arenaReady.push(() => {
                            lib.rank[info.rank].push(name);
                        });
                    }
                    if (info.des) {
                        lib.arenaReady.push(() => {
                            lib.characterIntro[name] = info.des;
                        });
                    }
                    if (info.catelogy /* && config[info.catelogy] */) {
                        lib.arenaReady.push(() => {
                            if (!lib.characterSort.mode_extension_rExtension_Rain) lib.characterSort.mode_extension_rExtension_Rain = {};
                            if (!lib.characterSort.mode_extension_rExtension_Rain[`rExtension_Rain_${info.catelogy}`]) lib.characterSort.mode_extension_rExtension_Rain[`rExtension_Rain_${info.catelogy}`] = Array.new();
                            lib.characterSort.mode_extension_rExtension_Rain[`rExtension_Rain_${info.catelogy}`].push(name);
                        })
                    }
                    if (info.replace) {
                        if (!lib.characterReplace[info.replace].contains(name)) lib.characterReplace[info.replace].push(name);
                    }
                }

                lib.translate.mode_extension_rExtension_Rain_character_config = "雨落同清";
                lib.translate.mode_extension_rExtension_Rain_Standard_character_config = "雨落同清";
                lib.translate["rExtension_Rain"] = "雨落同清";
                lib.translate["rExtension_Rain_Standard"] = "标准包";
            }
            // Rank Setting
            lib.arenaReady.push(() => {
                lib.rank.a.push("rExtension_Rain_Character_guojia");
            });
        },
        precontent: () => {

        },
        config: {},
        help: {},
        package: {
            character: {
                /*
                character: {
                    "rExtension_Rain_Character_guojia": ["male", "wei", 3,
                        ["rExtension_Rain_Skill_guojia_tiandu", "rExtension_Rain_Skill_guojia_yiji"],
                        [
                            "ext:雨落同清/image/character/rExtension_Rain_Character_guojia.jpg",
                            "die_audio"
                        ]
                    ]
                },
                characterSort: {
                    rExtension_Rain: {
                        rExtension_Rain_standard: ["rExtension_Rain_Character_guojia"]
                    }
                },
                characterIntro: {
                    "rExtension_Rain_Character_guojia": "字奉孝，颍川阳翟人，官至军师祭酒。惜天妒英才，英年早逝。有诗云：“良计环环不遗策，每临制变满座惊”"
                },
                translate: {
                    "rExtension_Rain_Character_guojia": "郭嘉",
                },
                */
                character: {},
                translate: {}
            },
            card: {
                card: {},
                translate: {},
                list: [],
            },
            skill: {
                skill: {
                    "rExtension_Rain_Skill_liubei_rende": {
                        audio: "ext:雨落同清/audio/skill:2",
                        enable: 'phaseUse',
                        filterCard: true,
                        selectCard: [1, Infinity],
                        discard: false,
                        lose: false,
                        init: (player, skill) => {
                            if (!player.storage[skill]) player.storage[skill] = 0;
                        },
                        filter: (event, player) => {
                            return player.hasCards("h");
                        },
                        delay: false,
                        filterTarget: function (card, player, target) {
                            return player != target;
                        },
                        check: function (card) {
                            if (ui.selected.cards.length && ui.selected.cards[0].name == 'du') return 0;
                            if (!ui.selected.cards.length && card.name == 'du') return 20;
                            var player = get.owner(card);
                            if (ui.selected.cards.length >= Math.max(2, player.countCards('h') - player.hp)) return 0;
                            if (player.hp == player.maxHp || player.storage["rExtension_Rain_Skill_liubei_rende"] < 0 || player.countCards('h') <= 1) {
                                var players = game.filterPlayer();
                                for (var i = 0; i < players.length; i++) {
                                    if (players[i].hasSkill('haoshi') &&
                                        !players[i].isTurnedOver() &&
                                        !players[i].hasJudge('lebu') &&
                                        get.attitude(player, players[i]) >= 3 &&
                                        get.attitude(players[i], player) >= 3) {
                                        return 11 - get.value(card);
                                    }
                                }
                                if (player.countCards('h') > player.hp) return 10 - get.value(card);
                                if (player.countCards('h') > 2) return 6 - get.value(card);
                                return -1;
                            }
                            return 10 - get.value(card);
                        },
                        content: function () {
                            'step 0'
                            var evt = _status.event.getParent("phaseUse");
                            if (evt && evt.name == "phaseUse" && !evt.rExtrende) {
                                var next = game.createEvent("rExtrende_clear");
                                _status.event.next.remove(next);
                                evt.after.push(next);
                                evt.rExtrende = true;
                                next.player = player;
                                next.setContent(() => {
                                    player.storage["rExtension_Rain_Skill_liubei_rende"] = 0;
                                });
                            }
                            target.gain(cards, player, "giveAuto");
                            if (typeof player.storage.rerende != 'number') {
                                player.storage[event.name] = 0;
                            }
                            if (player.storage[event.name] >= 0) {
                                player.storage[event.name] += cards.length;
                                if (player.storage[event.name] >= 2) {
                                    var list = [];
                                    if (lib.filter.cardUsable({ name: 'sha' }, player, event.getParent('chooseToUse')) && game.hasPlayer(function (current) {
                                        return player.canUse('sha', current);
                                    })) {
                                        list.push(['基本', '', 'sha']);
                                    }
                                    for (var i of lib.inpile_nature) {
                                        if (lib.filter.cardUsable({ name: 'sha', nature: i }, player, event.getParent('chooseToUse')) && game.hasPlayer(function (current) {
                                            return player.canUse({ name: 'sha', nature: i }, current);
                                        })) {
                                            list.push(['基本', '', 'sha', i]);
                                        }
                                    }
                                    if (lib.filter.cardUsable({ name: 'tao' }, player, event.getParent('chooseToUse')) && game.hasPlayer(function (current) {
                                        return player.canUse('tao', current);
                                    })) {
                                        list.push(['基本', '', 'tao']);
                                    }
                                    if (lib.filter.cardUsable({ name: 'jiu' }, player, event.getParent('chooseToUse')) && game.hasPlayer(function (current) {
                                        return player.canUse('jiu', current);
                                    })) {
                                        list.push(['基本', '', 'jiu']);
                                    }
                                    if (list.length) {
                                        player.chooseButton(['是否视为使用一张基本牌？', [list, 'vcard']]).set('ai', function (button) {
                                            var player = _status.event.player;
                                            var card = { name: button.link[2], nature: button.link[3] };
                                            if (card.name == 'tao') {
                                                if (player.hp == 1 || (player.hp == 2 && !player.hasShan()) || player.needsToDiscard()) {
                                                    return 5;
                                                }
                                                return 1;
                                            }
                                            if (card.name == 'sha') {
                                                if (game.hasPlayer(function (current) {
                                                    return player.canUse(card, current) && get.effect(current, card, player, player) > 0
                                                })) {
                                                    if (card.nature == 'fire') return 2.95;
                                                    if (card.nature == 'thunder' || card.nature == 'ice') return 2.92;
                                                    return 2.9;
                                                }
                                                return 0;
                                            }
                                            if (card.name == 'jiu') {
                                                return 0.5;
                                            }
                                            return 0;
                                        });
                                    }
                                    else {
                                        event.finish();
                                    }
                                    player.storage[event.name] = -1;
                                }
                                else {
                                    event.finish();
                                }
                            }
                            else {
                                event.finish();
                            }
                            'step 1'
                            if (result && result.bool && result.links[0]) {
                                var card = { name: result.links[0][2], nature: result.links[0][3] };
                                player.chooseUseTarget(card, true);
                            }
                            else {
                                if (player.hp < player.maxHp) player.recover();
                            }
                        },
                        ai: {
                            fireAttack: true,
                            order: function (skill, player) {
                                if (player.hp < player.maxHp && player.storage["rExtension_Rain_Skill_liubei_rende"] < 2 && player.countCards('h') > 1) {
                                    return 10;
                                }
                                return 4;
                            },
                            result: {
                                target: function (player, target) {
                                    if (target.hasSkillTag('nogain')) return 0;
                                    if (ui.selected.cards.length && ui.selected.cards[0].name == 'du') {
                                        if (target.hasSkillTag('nodu')) return 0;
                                        return -10;
                                    }
                                    if (target.hasJudge('lebu')) return 0;
                                    var nh = target.countCards('h');
                                    var np = player.countCards('h');
                                    if (player.hp == player.maxHp || player.storage["rExtension_Rain_Skill_liubei_rende"] < 0 || player.countCards('h') <= 1) {
                                        if (nh >= np - 1 && np <= player.hp && !target.hasSkill('haoshi')) return 0;
                                    }
                                    return Math.max(1, 5 - nh);
                                }
                            },
                            effect: {
                                target: function (card, player, target) {
                                    if (player == target && get.type(card) == 'equip') {
                                        if (player.countCards('e', { subtype: get.subtype(card) })) {
                                            if (game.hasPlayer(function (current) {
                                                return current != player && get.attitude(player, current) > 0;
                                            })) {
                                                return 0;
                                            }
                                        }
                                    }
                                }
                            },
                            threaten: 0.8
                        }
                    },
                    "rExtension_Rain_Skill_simayi_fankui": {
                        audio: "ext:雨落同清/audio/skill:2",
                        trigger: { player: "damageEnd" },
                        direct: true,
                        filter: function (event, player) {
                            return (event.source && event.source.countGainableCards(player, "he") && event.num > 0 && event.source != player);
                        },
                        content: function () {
                            "step 0"
                            event.count = trigger.num;
                            "step 1"
                            --event.count;
                            player.gainPlayerCard(get.prompt(event.name, trigger.source), trigger.source, get.buttonValue, "he").set("logSkill", [event.name, trigger.source]);
                            "step 2"
                            if (result.bool) {
                                player.draw(1, "nodelay");
                                if (event.count > 0 && trigger.source.countGainableCards(player, "he") > 0) event.goto(1);
                            }
                        },
                        ai: {
                            maixie_defend: true,
                            effect: {
                                target: function (card, player, target) {
                                    if (player.countCards("he") > 1 && get.tag(card, "damage")) {
                                        if (player.hasSkillTag("jueqing", false, target)) return [1, -1.5];
                                        if (get.attitude(target, player) < 0) return [1, 1];
                                    }
                                }
                            }
                        }
                    },
                    "rExtension_Rain_Skill_simayi_guicai": {
                        audio: "ext:雨落同清/audio/skill:2",
                        trigger: { global: "judge" },
                        direct: true,
                        filter: function (event, player) {
                            return player.countCards("hes") > 0;
                        },
                        content: function () {
                            "step 0"
                            player.chooseCard(get.translation(trigger.player) + "的" + (trigger.judgestr || "") + "判定为" +
                                get.translation(trigger.player.judging[0]) + "，" + get.prompt("rExtension_Rain_Skill_simayi_guicai"), "hes", function (card) {
                                    var player = _status.event.player;
                                    var mod2 = game.checkMod(card, player, "unchanged", "cardEnabled2", player);
                                    if (mod2 != "unchanged") return mod2;
                                    var mod = game.checkMod(card, player, "unchanged", "cardRespondable", player);
                                    if (mod != "unchanged") return mod;
                                    return true;
                                }).set("ai", function (card) {
                                    var trigger = _status.event.getTrigger();
                                    var player = _status.event.player;
                                    var judging = _status.event.judging;
                                    var result = trigger.judge(card) - trigger.judge(judging);
                                    var attitude = get.attitude(player, trigger.player);
                                    if (attitude == 0 || result == 0) return 0;
                                    if (attitude > 0) {
                                        return result - get.value(card) / 2;
                                    }
                                    else {
                                        return -result - get.value(card) / 2;
                                    }
                                }).set("judging", trigger.player.judging[0]);
                            "step 1"
                            if (result.bool) {
                                player.respond(result.cards, "rExtension_Rain_Skill_simayi_guicai", "highlight", "noOrdering");
                            }
                            else {
                                event.finish();
                            }
                            "step 2"
                            if (result.bool) {
                                player.gain(trigger.player.judging[0], "gain2");
                                if (trigger.judge(result.cards[0]) != trigger.judge(trigger.player.judging[0])) player.draw(1, "nodelay");
                                if (trigger.player.judging[0].clone) {
                                    trigger.player.judging[0].clone.classList.remove("thrownhighlight");
                                    game.broadcast(function (card) {
                                        if (card.clone) {
                                            card.clone.classList.remove("thrownhighlight");
                                        }
                                    }, trigger.player.judging[0]);
                                    game.addVideo("deletenode", player, get.cardsInfo([trigger.player.judging[0].clone]));
                                }
                                //game.cardsDiscard(trigger.player.judging[0]);
                                trigger.player.judging[0] = result.cards[0];
                                trigger.orderingCards.addArray(result.cards);
                                game.log(trigger.player, "的判定牌改为", result.cards[0]);
                            }
                            "step 3"
                            game.delay(2);
                        },
                        ai: {
                            rejudge: true,
                            tag: {
                                rejudge: 1,
                            }
                        }
                    },
                    "rExtension_Rain_Skill_guojia_tiandu": {
                        audio: "ext:雨落同清/audio/skill:2",
                        trigger: {
                            player: "judgeEnd",
                        },
                        filter: (event, player) => {
                            var bool1 = get.position(event.result.card, true) == "o";
                            var bool2 = event.result.bool;
                            return bool1 || !bool2;
                        },
                        frequent: (event) => {
                            if (event.result.card.name == "du") return false;
                            if (get.mode() == "guozhan") return false;
                            return true;
                        },
                        check: (event) => {
                            if (event.result.card.name == "du") return false;
                            return true;
                        },
                        direct: true,
                        content: () => {
                            "step 0"
                            var bool1 = get.position(trigger.result.card, true) == "o";
                            var bool2 = trigger.result.bool;
                            player.chooseBool(`###是否发动【${get.translation(event.name)}】（当前趋势：${bool2 == false ? "失利" : (bool2 == true ? "有利" : "无利")}）？###${bool1 ? (bool2 ? "当你的判定牌生效时，你可以获得之。" : get.translation(`${event.name}_info`)) : "当你的判定牌生效时，若此时判定趋势偏向失利或无利，你摸一张牌"}`).set("frequentSkill", event.name);
                            "step 1"
                            if (!result.bool) {
                                event.finish();
                                return;
                            }
                            player.logSkill(event.name);
                            if (get.position(trigger.result.card, true) == "o") {
                                player.gain(trigger.result.card, "gain2")
                            }
                            if (!trigger.result.bool) {
                                player.draw(1);
                            }
                        }
                    },
                    "rExtension_Rain_Skill_guojia_yiji": {
                        audio: "ext:雨落同清/audio/skill:2",
                        trigger: {
                            player: ["damageEnd", "loseHpEnd"],
                        },
                        frequent: true,
                        filter: (event) => {
                            return event.num > 0;
                        },
                        content: () => {
                            "step 0"
                            event.count = 1;
                            "step 1"
                            player.draw(3, "nodelay");
                            let bool = player.countCards("j") > 0;
                            player[bool ? "discardPlayerCard" : "chooseToDiscard"](bool ? player : "请弃置一张牌", true, 1, "hej");
                            event.given = 0;
                            event.temp = Array.new();
                            "step 2"
                            player.chooseCardTarget({
                                filterCard: true,
                                selectCard: [1, 2],
                                filterTarget: (card, player, target) => {
                                    return player != target && !event.temp.contains(target);
                                },
                                ai1: (card) => {
                                    if (ui.selected.cards.length > 0) return -1;
                                    if (card.name == "du") return 20;
                                    return (_status.event.player.countCards("h") - _status.event.player.hp);
                                },
                                ai2: (target) => {
                                    let att = get.attitude(_status.event.player, target);
                                    if (ui.selected.cards.length && ui.selected.cards[0].name == "du") {
                                        if (target.hasSkillTag("nodu")) return 0;
                                        return 1 - att;
                                    }
                                    return att - 4;
                                },
                                prompt: "请选择要送人的卡牌",
                                position: "he",
                            });
                            "step 3"
                            if (result.bool) {
                                player.line(result.targets, "green");
                                result.targets[0].gain(result.cards, player, "giveAuto");
                                event.given += result.cards.length;
                                event.cards = result.cards;
                                event.target = result.targets[0];
                                player.chooseCardButton("选择任意张牌置于其武将牌上", false, event.cards, [1, event.cards.length]).set("ai", button => {
                                    if (ui.selected.buttons.length == 0) return 1;
                                    return 0;
                                });
                            }
                            else if (event.count < trigger.num) {
                                event.temp = Array.new();
                                ++event.count;
                                event.goto(5);
                            }
                            else event.finish();
                            "step 4"
                            if (result.bool) {
                                var cards = result.links.dup();
                                event.target.lose(cards, ui.special, "toStorage");
                                if (!event.target.storage["rExtension_Rain_Skill_guojia_yiji_storage"]) {
                                    event.target.addSkill("rExtension_Rain_Skill_guojia_yiji_storage");
                                    event.target.storage["rExtension_Rain_Skill_guojia_yiji_storage"] = Array.new();
                                }
                                event.target.storage["rExtension_Rain_Skill_guojia_yiji_storage"] = event.target.storage["rExtension_Rain_Skill_guojia_yiji_storage"].concat(cards);
                                game.addVideo("toStorage", event.target, ["rExtension_Rain_Skill_guojia_yiji_storage", get.cardsInfo(event.target.storage["rExtension_Rain_Skill_guojia_yiji_storage"]), "cards"]);
                            }
                            if (event.given < 2) {
                                event.temp.push(event.target);
                                delete event.target;
                                delete event.cards;
                                event.goto(2);
                            }
                            else if (event.count < trigger.num) {
                                event.temp = Array.new();
                                delete event.target;
                                delete event.cards;
                                ++event.count;
                            }
                            else event.finish();
                            "step 5"
                            player.chooseBool(get.prompt2(event.name)).set("frequentSkill", event.name);
                            "step 6"
                            if (result.bool) {
                                player.logSkill(event.name);
                                event.goto(1);
                            }
                        },
                        ai: {
                            maixie: true,
                            maixie_hp: true,
                            result: {
                                effect: (card, player, target) => {
                                    if (get.tag(card, "damage")) {
                                        // if(player.hasSkillTag("jueqing", false, target)) return [1,-2];
                                        if (!target.hasFriend()) return;
                                        let num = 1;
                                        if (get.attitude(player, target) > 0) {
                                            if (player.needsToDiscard()) {
                                                num = 0.7;
                                            }
                                            else {
                                                num = 0.5;
                                            }
                                        }
                                        if (player.hp >= 4) return [1, num * 2];
                                        if (target.hp == 3) return [1, num * 1.5];
                                        if (target.hp == 2) return [1, num * 0.5];
                                    }
                                }
                            },
                            threaten: 0.6
                        }
                    },
                    "rExtension_Rain_Skill_guojia_yiji_storage": {
                        trigger: {
                            player: "phaseUseBegin"
                        },
                        forced: true,
                        mark: true,
                        popup: "遗计拿牌",
                        audio: false,
                        content: () => {
                            player.$draw(player.storage["rExtension_Rain_Skill_guojia_yiji_storage"].length);
                            player.gain(player.storage["rExtension_Rain_Skill_guojia_yiji_storage"], "fromStorage");
                            delete player.storage["rExtension_Rain_Skill_guojia_yiji_storage"];
                            player.removeSkill("rExtension_Rain_Skill_guojia_yiji_storage");
                            game.delay();
                        },
                        intro: {
                            content: "cardCount"
                        }
                    }
                },
                translate: {
                    "rExtension_Rain_Skill_liubei_rende": "仁德",
                    "rExtension_Rain_Skill_liubei_rende_info": "出牌阶段，你可以将至少一张手牌交给一名其他角色；若你于此阶段内已给出两张以上的牌，你回复一点体力或视为使用一张基本牌。",
                    "rExtension_Rain_Skill_liubei_jijiang": "激将",
                    "rExtension_Rain_Skill_liubei_jijiang_info": "当你需要使用或打出【杀】时，你可以令其他蜀势力角色依次选择是否打出一张【杀】，若有角色响应，则你视为使用或打出了此【杀】，若你的身份为主公，其摸一张牌；当有蜀势力角色于回合外使用或打出【杀】时，其可以令你摸一张牌，若你的身份为主公，其摸一张牌。",

                    "rExtension_Rain_Skill_simayi_fankui": "反馈",
                    "rExtension_Rain_Skill_simayi_fankui_info": "当你受到一点伤害时，你可以获得伤害来源的一张牌并摸一张牌。",
                    "rExtension_Rain_Skill_simayi_guicai": "鬼才",
                    "rExtension_Rain_Skill_simayi_guicai_info": "当一名角色的判定牌即将生效时，你可以打出一张牌替代之；若判定趋势因此改变，你摸一张牌。",

                    "rExtension_Rain_Skill_guojia_tiandu": "天妒",
                    "rExtension_Rain_Skill_guojia_tiandu_info": "当你的判定牌生效后，你可以获得之。若此时判定趋势偏向失利或无利，你摸一张牌。",
                    "rExtension_Rain_Skill_guojia_yiji": "遗计",
                    "rExtension_Rain_Skill_guojia_yiji_storage": "遗计",
                    "rExtension_Rain_Skill_guojia_yiji_info": "当你受到一点伤害或失去一点体力后，你可以摸三张牌并弃置区域里的一张牌，然后你可以将至多两张牌分配给其他角色；当你以此法将一张牌交给其他角色时，你可将此牌至于其武将牌上，令其于出牌阶段开始时获得。"
                },
            },
            intro: intro,
            author: "Rintim",
            diskURL: "",
            forumURL: "",
            version: "0.1.0",
        },
        files: {
            "character": [],
            "card": [],
            "skill": []
        }
    }
    return extension;
});