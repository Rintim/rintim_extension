// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From Theopse (Self@theopse.org)
// Licensed under BSD-2-Caluse
// File: extension.js (rExtension/reHeart/extension.js)
// Content:  
// Copyright (c) 2022 Theopse Organization All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

"use strict"

/*
lib.config.extensions
lib.config.extension_RitySelf_enable
*/

game.import("extension", (lib,game,ui,get,ai,_status) => {
    var intro = "</br><span style=\"color:#1688F2\">rExtension Runtime from Rintim</span></br><span style=\"color:#FF3333\">Development Branch</span></br></br>"
    var extension =  {
        name: "reHeart",
        editable: false,
        content: (config,pack) => {
            // Initialize
            if (lib.storage["reHeart"]) return;
            lib.storage["reHeart"] = true;
            // Add Key Group
            lib.arenaReady.push(() => {
                if (!lib.group.contains("key")) lib.group.add("key");
            });
            // Create Methods
            {
                // Array
                {
                    Array.new = function () {
                        return new Array(...arguments);
                    };

                    Array.seq = function (start, end, step) {
                        // if (!typeof start === "number" || !start.isInteger()) return;
                        if (!end) return Array.seq(0, start);
                        // if (!typeof end === "number" || !end.isInteger()) return;
                        if (!step) return Array.seq(start, end, /* start >= end ? */ 1 /* : -1 */);
                        // if (!typeof step === "number" || !step.isInteger()) return;
                        let result = Array.new();
                        for (var i = start; i <= end; i += step) result.push(i);
                        return result;
                    };

                    Array.prototype.first = function() {
                        return this[0];
                    };

                    Array.prototype.head = Array.prototype.first;

                    Array.prototype.last = function() {
                        return this[this.length - 1];
                    };

                    Array.prototype.tail = function() {
                        return this.slice(1);
                    };

                    Array.prototype.is_empty = function() {
                        return !this.length;
                    };

                    Array.prototype.isEmpty = Array.prototype.is_empty;

                    Array.prototype.dup = function() {
                        return /* this.map(item => item) */ this.slice(0);
                    };

                    Array.prototype.decrease = function(acc, fun) {
                        if (!fun) return this.tail().decrease(this[0], acc);
                        if (this.is_empty()) return acc;
                        return this.tail().decrease(fun(this[0], acc), fun);
                    };

                    Array.prototype.count = function(fun) {
                        return this.filter(fun).length;
                    };
                }

                // Number
                {
                    Number.new = function () {
                        return new Number(...arguments);
                    };

                    Number.prototype.is_integer = function () {
                        return Number.isInteger(this.valueOf());
                    };

                    Number.prototype.isInteger = Number.prototype.is_integer;

                    Number.prototype.is_even = function(fun) {
                        if (!this.is_integer()) return -1;
                        return !(this & 1);
                    };

                    Number.prototype.isEven = Number.prototype.is_even;

                    Number.prototype.times = function(fun) {
                        if (this < 0) return;
                        if (!this.is_integer()) return;
                        for (let i = 0; i < this; ++i) {
                            fun(i);
                        }
                        return 0;
                    };
                }
            }
        },
        precontent: () => {
            
        },
        config: {},
        help: {},
        package: {
            character:{
                character: {},
                translate: {},
            },
            card: {
                card: {},
                translate: {},
                list: [],
            },
            skill:{
                skill: {},
                translate: {},
            },
            intro: intro,
            author: "Rintim",
            diskURL: "",
            forumURL: "",
            version: "0.1.0",
        },
        files:{
            "character": [],
            "card": [],
            "skill": []
        }
    }
    return extension;
});