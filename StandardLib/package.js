// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From Theopse (Self@theopse.org)
// Licensed under BSD-2-Caluse
// File: package.js (rExtension/StandardLib/package.js)
// Content:  
// Copyright (c) 2022 Theopse Organization All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

"use strict"

extension["StandardLib"] = {
	intro: "无名杀 rExtension 标准代码库",
	author: "Rintim",
	netdisk: "",
	forum: "",
	version: "0.1.0",
	files: ["extension.js"],
	size: "110KB"
};
